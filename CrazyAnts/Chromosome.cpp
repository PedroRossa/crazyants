#include "Chromosome.h"

#pragma region Constructors

Chromosome::Chromosome() : Chromosome(0)
{
}

//size - Size of the vector
Chromosome::Chromosome(int size)
{
	this->size = size;
	Initialize();
}

//size - Size of the vector
//directions - vector with the directions, if is an empty vector, the system gonna create a random vector
Chromosome::Chromosome(int size, vector<int> directions)
{
	this->size = size;
	this->directions = directions;
}

Chromosome::~Chromosome()
{
}

#pragma endregion

#pragma region Gets

int Chromosome::GetSize() { return this->size; }
vector<int> Chromosome::GetDirections() { return this->directions; }
int Chromosome::GetDirection(int pos) { return this->directions[pos]; }

void Chromosome::SetDirections(vector<int> directions){ this->directions = directions; }

#pragma endregion

#pragma region Public Methods

void Chromosome::Initialize()
{
	for (size_t i = 0; i < this->size; i++)
	{
		Direction randDir = RandDirection();
		directions.push_back(randDir);
	}
}

#pragma endregion