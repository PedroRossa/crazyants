#ifndef ANT_H
#define ANT_H

#include "Individual.h"
#include "Map.h"
class Ant : public Individual
{
private:

#pragma region Attrbitutes

	int hp;
	int atualHP;

	float distanceToAntHouse;

	Map* map;

#pragma endregion

public:

#pragma region Constructors

	Ant();
	Ant(size_t id, vec2 birthPos, int generation, int hp, Map* map, Chromosome chromosome);
	~Ant();

#pragma endregion

#pragma region Gets

	int GetHP();
	int GetAtualHP();

	float GetDistanceToAntHouse();

	Map* GetMap();

	void SetAtualHP(int hp);

#pragma endregion

#pragma region Public Methods

	bool Move();
	bool Move(Direction dir);

	float UpdateFitness();
	void DecreaseHP();

#pragma endregion
	
};

#endif