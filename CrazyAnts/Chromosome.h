#ifndef CHROMOSOME_H
#define CHROMOSOME_H

#include "Helper.h"

using namespace std;

class Chromosome
{
private:

#pragma region Attributes

	int size;
	vector<int> directions;

#pragma endregion

public:

#pragma region Constructors

	Chromosome();
	Chromosome(int size);
	Chromosome(int size, vector<int> directions);
	~Chromosome();

#pragma endregion

#pragma region Gets

	int GetSize();
	vector<int> GetDirections();
	int GetDirection(int pos);

	void SetDirections(vector<int> directions);

#pragma endregion

#pragma region Public Methods

	void Initialize();

#pragma endregion

};

#endif