#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include "Helper.h"
#include "Chromosome.h"

class Individual
{
protected:

#pragma region Attributes

	size_t id;
	vec2 pos;
	vec2 initPos;
	int generation;
	float fitness;

	Chromosome chromosome;

#pragma endregion

public:

#pragma region Constructors

	Individual();
	~Individual();

#pragma endregion

#pragma region Gets

	size_t GetId();
	vec2 GetPos();
	vec2 GetInitPos();
	int GetGeneration();
	float GetFitness();

	Chromosome GetChromosome();

	void SetPos(vec2 pos);
	void SetFitness(float fitness);
	void SetChromosome(Chromosome chromosome);

#pragma endregion
	
};

#endif