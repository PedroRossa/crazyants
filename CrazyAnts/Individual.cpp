#include "Individual.h"

#pragma region Constructors

Individual::Individual()
{
}

Individual::~Individual()
{
}

#pragma endregion

#pragma region Gets

size_t Individual::GetId() { return this->id; }
vec2 Individual::GetPos() { return this->pos; }
vec2 Individual::GetInitPos() { return this->initPos; }
int Individual::GetGeneration() { return this->generation; }
float Individual::GetFitness() { return this->fitness; }

Chromosome Individual::GetChromosome() { return this->chromosome; }


void Individual::SetPos(vec2 pos) { this->pos = pos; }
void Individual::SetFitness(float fitness) { this->fitness = fitness; }

void Individual::SetChromosome(Chromosome chromosome) { this->chromosome = chromosome; }

#pragma endregion