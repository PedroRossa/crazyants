#include <iostream>
#include <SFML/Graphics.hpp>
#include "World.h"

int main()
{
	srand(time(NULL));

	WorldConfig config(20, 1000, 35, 200, 1, true, 4);
	World world(config, 350);

	cout << "Press a key to start!" << endl;

	system("pause");

	world.Start();

	world.Update();

	return 0;
}