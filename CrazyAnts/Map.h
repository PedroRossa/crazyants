#ifndef MAP_H
#define MAP_H

#include "Helper.h"
#include <SFML\Graphics.hpp>

class Map
{
private:

#pragma region Attributes

	size_t width;
	size_t height;
	int** matrix;
	vec2 antHousePos;
	vec2 birthPos;

	int maxDistance;

	sf::Texture texture;
	sf::Sprite sprite;

#pragma endregion

#pragma region Private Methods

	void InitializeMatrix();
	bool InitializeTexture();

	void UpdateTexture();

#pragma endregion

public:

#pragma region Constructors

	Map();
	Map(size_t width, size_t height, vec2 antHousePos, vec2 birthPos);
	~Map();

#pragma endregion

#pragma region Gets

	size_t GetHeight();
	size_t GetWidth();
	int** GetMatrix();
	vec2 GetAntHousePos();
	vec2 GetBirthPos();

	int GetMaxDistance();

	sf::Texture GetTexture();
	sf::Sprite GetSprite();
	
	TileType GetMatrixValue(vec2 pos);
	TileType GetMatrixValue(size_t posX, size_t posY);

	void SetMatrixPos(vec2 pos, TileType value);
	void SetMatrixPos(size_t posX, size_t posY, TileType value);

#pragma enderegion

#pragma region Public Methods

	void Update();

#pragma endregion

};

#endif