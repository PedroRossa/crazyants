#include "Genetic.h"

#pragma region Constructors

Genetic::Genetic()
{
}

Genetic::~Genetic()
{
}

#pragma endregion

#pragma region Private Methods

vector<Ant*> Genetic::Roulette_Selection(vector<Ant*> population, WorldConfig configs)
{
	int sum = 0;
	int index = 0;

	vector<Ant*> newPop;

	sort(population.begin(), population.end(), [](Ant* const& a, Ant* const& b) { return a->GetFitness() < b->GetFitness(); });

	int populationSize = population.size();

	//check for elitism
	if (configs.UseElitism())
	{
		//get the best fitness individuals
		for (size_t i = 0; i < configs.GetElitismFactor(); i++)
		{
			Ant* ant = new Ant(
				i,
				population[i]->GetInitPos(),
				population[i]->GetGeneration() + 1,
				population[i]->GetHP(),
				population[i]->GetMap(),
				population[i]->GetChromosome());

			newPop.push_back(ant);
		}

		populationSize -= configs.GetElitismFactor();
	}

	for (unsigned int i = 0; i < population.size(); i++) {
		sum += population[i]->GetFitness();
	}

	for (size_t i = 0; i < populationSize; i++)
	{
		double rnd = (((double)rand() / (double)RAND_MAX) * sum);
		sum = 0;

		for (unsigned int i = 0; i < population.size(); i++) {
			sum += population[i]->GetFitness();
			if (rnd < sum) {
				index = i;
				break;
			}
		}

		Ant* ant;
		//correct the ID
		if (configs.UseElitism())
		{
			ant = new Ant(
				i + configs.GetElitismFactor(),
				population[index]->GetInitPos(),
				population[index]->GetGeneration() + 1,
				population[index]->GetHP(),
				population[index]->GetMap(),
				population[index]->GetChromosome());
		}
		else
		{
			ant = new Ant(
				i,
				population[index]->GetInitPos(),
				population[index]->GetGeneration() + 1,
				population[index]->GetHP(),
				population[index]->GetMap(),
				population[index]->GetChromosome());
		}
		newPop.push_back(ant);
	}

	/*

	int totalFitness = 0;

	//Calculate Total Fitness
	for (size_t i = 0; i < population.size(); i++)
		totalFitness += population[i]->GetFitness();

	vector<Ant*> newPopulation;
	//create the new Population
	for (size_t i = 0; i < population.size(); i++)
	{
		int randVal = rand() % totalFitness;
		int k = 0;
		while (randVal > 0)
		{
			randVal -= population[k]->GetFitness();
			k++;
		}

		if (k == 0)
			k++;
	

		Ant* ant = new Ant(
			i,
			population[k - 1]->GetInitPos(),
			population[k - 1]->GetGeneration() + 1,
			population[k - 1]->GetHP(),
			population[k - 1]->GetMap(),
			population[k - 1]->GetChromosome());

		newPopulation.push_back(ant);
	}
	*/


	//Run the crossover and mutation
	for (size_t i = 0; i < newPop.size(); i+=2)
	{
		Chromosome* newChromos = Crossover(newPop[i]->GetChromosome(), newPop[i + 1]->GetChromosome(), configs.GetCrossoverTax(), configs.GetCrossoverDivision());

		Mutation(&newChromos[0], configs.GetMutationTax());
		Mutation(&newChromos[1], configs.GetMutationTax());

		newPop[i]->SetChromosome(newChromos[0]);
		newPop[i]->SetChromosome(newChromos[1]);
	}

	return newPop;
}

vector<int> Genetic::GenerateDivisionPoints(int numberOfDivisions, int sizeOfChromossome)
{
	vector<int> divisionValues;

	//generate Division points
	for (size_t i = 0; i < numberOfDivisions; i++)
	{
		int randDivisionPoint = 1 + (rand() % (sizeOfChromossome - 1));
		//if find the rand value inside of divisionValues vector, try again
		if (find(divisionValues.begin(), divisionValues.end(), randDivisionPoint) != divisionValues.end())
		{
			i--;
		}
		else
		{
			divisionValues.push_back(randDivisionPoint);
		}
	}

	sort(divisionValues.begin(), divisionValues.end());
	return divisionValues;
}

Chromosome* Genetic::MixChromosomes(vector<int>valuesA, vector<int> valuesB, vector<int> divisionPoints)
{
	vector<int> newAValues;
	vector<int> newBValues;

	int globalPosition = 0;
	bool isPairCross = true;
	int atualDivision = 0;
	int lastDivision = 0;

	for (size_t i = 0; i < divisionPoints.size(); i++)
	{
		atualDivision = divisionPoints[i];
		
		if (i > 0)
			lastDivision = divisionPoints[i - 1];

		for (size_t j = lastDivision; j < atualDivision; j++)
		{
			if (isPairCross)
			{
				newAValues.push_back(valuesA[globalPosition]);
				newBValues.push_back(valuesB[globalPosition]);
			}
			else
			{
				newAValues.push_back(valuesB[globalPosition]);
				newBValues.push_back(valuesA[globalPosition]);
			}
			globalPosition++;
		}
		isPairCross = !isPairCross;
	}

	//set the last values
	for (size_t i = globalPosition; i < valuesA.size(); i++)
	{
		if (isPairCross)
		{
			newAValues.push_back(valuesA[i]);
			newBValues.push_back(valuesB[i]);
		}
		else
		{
			newAValues.push_back(valuesB[i]);
			newBValues.push_back(valuesA[i]);
		}
	}

	Chromosome a(newAValues.size(), newAValues);
	Chromosome b(newBValues.size(), newBValues);

	Chromosome* newChromosomes = new Chromosome[2];

	newChromosomes[0] = a;
	newChromosomes[1] = b;

	return newChromosomes;
}

#pragma endregion

#pragma region Public Methods

void Genetic::GenerateNewPopulation(vector<Ant*>& population, PopulationGeneratorType type, WorldConfig configs)
{
	switch (type)
	{
	case ROULETTE:
		population = Roulette_Selection(population, configs);
		break;
	default:
		break;
	}
}

Chromosome* Genetic::Crossover(Chromosome chromosomeA, Chromosome chromosomeB, int crossoverTax, int divisions)
{
	//garantee the max division possible
	if (divisions >= chromosomeA.GetSize());
		divisions = chromosomeA.GetSize() - 1;
	if (divisions <= 0)
		divisions = 1;

	vector<int> divisionPoints = GenerateDivisionPoints(divisions, chromosomeA.GetSize());

	int crossValue = rand() % 100;
	if (crossValue <= crossoverTax)
	{
		Chromosome* c = MixChromosomes(chromosomeA.GetDirections(), chromosomeB.GetDirections(), divisionPoints);
		return  new Chromosome[2]{ c[0], c[1] };
	}
	else
	{
		return new Chromosome[2]{ chromosomeA, chromosomeB };
	}
}

void Genetic::Mutation(Chromosome* chromosome, int mutationTax)
{
	vector<int> values = chromosome->GetDirections();
	
	for (size_t i = 0; i < values.size(); i++)
	{
		int mutationsValue = rand() % 100;
		if (mutationsValue <= mutationTax)
		{
			values[i] = RandDirection();
		}
	}

	chromosome->SetDirections(values);
}

#pragma endregion