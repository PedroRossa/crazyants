#include "Map.h"

#pragma region Constructors

Map::Map() : Map(400, 300, vec2(200, 50), vec2(200, 299))
{
}

Map::Map(size_t width, size_t height, vec2 antHousePos, vec2 birthPos)
{
	this->width = width;
	this->height = height;
	this->antHousePos = antHousePos;
	this->birthPos = birthPos;
	this->sprite = sf::Sprite();
	
	InitializeMatrix();
	SetMatrixPos(antHousePos, TileType::ANT_HOUSE);
	SetMatrixPos(birthPos, TileType::ANT_BIRTH);

	if (!InitializeTexture())
	{
		//Error on intialize texture
	}
}

Map::~Map()
{
}

#pragma endregion

#pragma region Private Methods

void Map::InitializeMatrix()
{
	this->matrix = new int*[this->width];
	for (size_t i = 0; i < this->width; i++)
	{
		this->matrix[i] = new int[this->height];

		for (size_t j = 0; j < this->height; j++)
		{
			this->matrix[i][j] = TileType::SAND;
		}
	}

	this->maxDistance = sqrt(pow(width, 2) + pow(height, 2));
}

bool Map::InitializeTexture()
{
	if (!texture.create(width, height))
	{
		// error...
		return false;
	}
	return true;
}

void Map::UpdateTexture()
{
	sf::Image* image = new sf::Image();
	image->create(width, height);

	for (size_t i = 0; i < height; i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			sf::Color color = GetTileColor((TileType)GetMatrixValue(j, i));
			image->setPixel(j, i, color);
		}
	}

	texture.update(*image);
	sprite.setTexture(texture);

	image->~Image();
}

#pragma endregion

#pragma region Gets

size_t Map::GetHeight() { return this->height; }
size_t Map::GetWidth() { return this->width; }
int** Map::GetMatrix() { return this->matrix; }
vec2 Map::GetAntHousePos() { return this->antHousePos; }
vec2 Map::GetBirthPos() { return this->birthPos; }

int Map::GetMaxDistance() { return this->maxDistance; }

sf::Texture Map::GetTexture() { return this->texture; }
sf::Sprite Map::GetSprite() { return this->sprite; }

#pragma enderegion

#pragma region Public Methods

TileType Map::GetMatrixValue(vec2 pos) { return (TileType)this->matrix[pos.x][pos.y]; }
TileType Map::GetMatrixValue(size_t posX, size_t posY) { return (TileType)this->matrix[posX][posY]; }

void Map::SetMatrixPos(vec2 pos, TileType value){ this->matrix[pos.x][pos.y] = value; }
void Map::SetMatrixPos(size_t posX, size_t posY, TileType value) { this->matrix[posX][posY] = value; }

void Map::Update()
{
	UpdateTexture();
}

#pragma endregion
