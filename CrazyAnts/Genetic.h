#ifndef GENETIC_H
#define GENETIC_H

#include "Ant.h"
#include "WorldConfig.h"

//TODO: N�o consegui fazer passar vector de classe Parent aqui, ent�o por enquanto estou passando
//Direto a classe filha

class Genetic
{
private:

#pragma region Private Methods

	vector<Ant*> Roulette_Selection(vector<Ant*> population, WorldConfig configs);

	vector<int> GenerateDivisionPoints(int numberOfDivisions, int sizeOfChromossome);

	Chromosome* MixChromosomes(vector<int>valuesA, vector<int> valuesB, vector<int> divisionPoints);

#pragma endregion

public:

#pragma region Constructors

	Genetic();
	~Genetic();

#pragma endregion

#pragma region Public Methods

	void GenerateNewPopulation(vector<Ant*>& population, PopulationGeneratorType type, WorldConfig configs);

	//Return two new chromosomes
	Chromosome* Crossover(Chromosome chromosomeA, Chromosome chromosomeB, int crossoverTax, int divisions = 1);
	void Mutation(Chromosome* chromosome, int mutationTax);

#pragma endregion

};

#endif