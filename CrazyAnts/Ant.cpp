#include "Ant.h"

#pragma region Constructors

Ant::Ant() : Ant(0, vec2(0, 0), 0, 0, nullptr, Chromosome())
{
}

Ant::Ant(size_t id, vec2 pos, int generation, int hp, Map* map, Chromosome chromosome)
{
	this->id = id;
	this->pos = pos;
	this->initPos = pos;
	this->generation = generation;
	this->chromosome = chromosome;

	this->map = map;
	this->hp = hp;
	this->atualHP = hp;
}

Ant::~Ant()
{
}

#pragma endregion

#pragma region Gets

int Ant::GetHP() { return this->hp; }
int Ant::GetAtualHP() { return this->atualHP; }

float Ant::GetDistanceToAntHouse() 
{
	this->distanceToAntHouse = CalculateEuclidianDistance(this->pos, map->GetAntHousePos());
	return this->distanceToAntHouse; 
}

Map* Ant::GetMap() { return this->map; }


void Ant::SetAtualHP(int hp) { this->atualHP = hp; }

#pragma endregion

#pragma region Public Methods

bool Ant::Move()
{
	Direction dir = (Direction)chromosome.GetDirection(hp - atualHP);
	vec2 newPos = pos;

	switch (dir)
	{
	case NW:
		newPos.x -= 1;
		newPos.y += 1;
		break;
	case N:
		newPos.y += 1;
		break;
	case NE:
		newPos.x += 1;
		newPos.y += 1;
		break;
	case W:
		newPos.x -= 1;
		break;
	case E:
		newPos.x += 1;
		break;
	case SW:
		newPos.x -= 1;
		newPos.y -= 1;
		break;
	case S:
		newPos.y -= 1;
		break;
	case SE:
		newPos.x += 1;
		newPos.y -= 1;
		break;
	default:
		break;
	}

	if (newPos.x >= 0 && newPos.x < map->GetWidth() &&
		newPos.y >= 0 && newPos.y < map->GetHeight())
	{
		this->pos = newPos;
		return true;
	}
	else
	{
		return false;
	}
}

bool Ant::Move(Direction dir)
{
	vec2 newPos = pos;

	switch (dir)
	{
	case NW:
		newPos.x -= 1;
		newPos.y += 1;
		break;
	case N:
		newPos.y += 1;
		break;
	case NE:
		newPos.x += 1;
		newPos.y += 1;
		break;
	case W:
		newPos.x -= 1;
		break;
	case E:
		newPos.x += 1;
		break;
	case SW:
		newPos.x -= 1;
		newPos.y -= 1;
		break;
	case S:
		newPos.y -= 1;
		break;
	case SE:
		newPos.x += 1;
		newPos.y -= 1;
		break;
	default:
		break;
	}

	if (newPos.x >= 0 && newPos.x < map->GetWidth() &&
		newPos.y >= 0 && newPos.y < map->GetHeight())
	{
		this->pos = newPos;
		return true;
	}
	else
	{
		return false;
	}
}

float Ant::UpdateFitness()
{
	float dis = GetDistanceToAntHouse();
	float value = dis / map->GetMaxDistance();
	
	//this->fitness = 1 - value;
	this->fitness = value;
	return this->fitness;
}

void Ant::DecreaseHP() { this->atualHP--; }

#pragma endregion
