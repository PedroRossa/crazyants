#include "World.h"

#pragma region Constructors

World::World()
{
}

World::World(WorldConfig configs, size_t cycles)
{
	this->configurations = configs;
	this->atualGeneration = 0;
	this->cycles = cycles;

	map = new Map();

	this->window = new sf::RenderWindow(sf::VideoMode(map->GetWidth(), map->GetHeight()), "Crazy Ants");
}

World::~World()
{
}

#pragma endregion

#pragma region Private Methods

void World::InternUpdate()
{
	if (atualGeneration < configurations.GetGenerations())
	{
		if (atualCycle < cycles)
		{
			for (size_t i = 0; i < ants.size(); i++)
			{
				//Clear atual ant position if not a older dead ant
				if (map->GetMatrixValue(ants[i]->GetPos()) != TileType::DEAD_ANT)
				{
					map->SetMatrixPos(ants[i]->GetPos(), TileType::WALKEDSAND);
				}

				ants[i]->Move();
				ants[i]->DecreaseHP();

				if (ants[i]->GetPos().x == map->GetAntHousePos().x && ants[i]->GetPos().y == map->GetAntHousePos().y)
				{
					foundAntHouse = new Ant(*ants[i]);
					endOfSimulation = true;
					return;
				}

				//Clear atual ant position if not a older dead ant
				if (map->GetMatrixValue(ants[i]->GetPos()) != TileType::DEAD_ANT)
				{
					map->SetMatrixPos(ants[i]->GetPos(), TileType::ANT);
				}
			}
			atualCycle++;
		}
		else
		{			
			atualCycle = 0;
			atualGeneration++;

			//Calculate Fitness
			for (size_t i = 0; i < ants.size(); i++)
			{
				float ft = ants[i]->UpdateFitness();

				if (ft <= 0)
				{
					endOfSimulation = true;
					return;
				}
			
				//Colored the ant as DEAD
				map->SetMatrixPos(ants[i]->GetPos(), TileType::DEAD_ANT);
			}

			sort(ants.begin(), ants.end(), [](Ant* const& a, Ant* const& b) { return a->GetFitness() < b->GetFitness(); });
			float bf = ants[0]->GetFitness();

			cout << "End of Generation: " << atualGeneration << " Proximity %: " << 1-bf << endl;

			genetic.GenerateNewPopulation(ants, PopulationGeneratorType::ROULETTE, configurations);
		}
	}
	else
	{
		endOfSimulation = true;
	}
}

void World::EventUpdate()
{
	sf::Event event;
	while (window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window->close();
	}
}

#pragma endregion

#pragma region Public Methods

void World::Start()
{
	endOfSimulation = false;

	for (size_t i = 0; i < configurations.GetPopulationSize(); i++)
	{
		Ant* a = new Ant(i, map->GetBirthPos(), atualGeneration, cycles, map, Chromosome(cycles));
		ants.push_back(a);
	}

	clock.restart();
	atualCycle = 0;
}

void World::Update()
{
	sf::Time elapsedTime;

	while (window->isOpen())
	{
		EventUpdate();

		//elapsedTime = clock.getElapsedTime();
		//if (elapsedTime.asMicroseconds() >= 1)
		//{}
		//	clock.restart();
				
		if (!endOfSimulation)
		{
			InternUpdate();
		}
		else
		{
			foundAntHouse->SetPos(foundAntHouse->GetInitPos());
			for (size_t i = 0; i < foundAntHouse->GetChromosome().GetSize(); i++)
			{
				map->SetMatrixPos(foundAntHouse->GetPos(), TileType::PATH_FOUND);
				foundAntHouse->Move((Direction)foundAntHouse->GetChromosome().GetDirection(i));
			}
		}

		if (atualGeneration % 10 == 0 || endOfSimulation)
		{
			map->Update();

			window->clear();
			window->draw(map->GetSprite());
			window->display();
		}

		if (endOfSimulation)
		{
			cout << "END OF THE SIMULATION" << endl;
			system("pause");
		}
	}
}

#pragma endregion
