#ifndef WORLD_H
#define WORLD_H

#include <SFML/Graphics.hpp>

#include "Ant.h"
#include "Map.h"
#include "WorldConfig.h"
#include "Genetic.h"

class World
{
private:

#pragma region Attributes

	sf::RenderWindow* window;
	sf::Clock clock;

	WorldConfig configurations;
	Map* map;
	vector<Ant*> ants;

	size_t cycles;
	size_t atualCycle;
	size_t atualGeneration;
	
	Ant* foundAntHouse;

	bool endOfSimulation;


	Genetic genetic;

#pragma endregion

#pragma region Private Methods

	void InternUpdate();

	void EventUpdate();

#pragma endregion

public:

#pragma region Constructors

	World();
	World(WorldConfig configs, size_t cycles);
	~World();

#pragma endregion

#pragma region Public Methods

	void Start();
	void Update();

#pragma endregion

};

#endif