#ifndef HELPER_H
#define HELPER_H

#include <map>
#include <iostream>
#include <SFML\Graphics.hpp>
#include <math.h>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;

struct vec2
{
	int x;
	int y;

	vec2()
	{
		this->x = 0;
		this->y = 0;
	}

	vec2(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

enum TileType
{
	NOTHING = 0,
	ANT = 1,
	ANT_HOUSE = 2,
	ANT_BIRTH = 3,
	SAND = 4,
	WALKEDSAND = 5,
	DEAD_ANT = 6,
	PATH_FOUND = 7
};

enum Direction
{
	NW = 0,
	N = 1,
	NE = 2,
	W = 3,
	E = 4,
	SW = 5,
	S = 6,
	SE = 7
};

enum PopulationGeneratorType
{
	ROULETTE = 0
};



static sf::Color GetTileColor(TileType value)
{
	switch (value)
	{
	case TileType::ANT:
		return sf::Color::Red;
	case TileType::ANT_BIRTH:
		return sf::Color::Blue;
	case TileType::ANT_HOUSE:
		return sf::Color::Magenta;
	case TileType::NOTHING:
		return sf::Color::Black;
	case TileType::SAND:
		return sf::Color(67, 42, 19);
	case TileType::WALKEDSAND:
		return sf::Color(160, 82, 45);
	case TileType::DEAD_ANT:
		return sf::Color::Black;
	case TileType::PATH_FOUND:
		return sf::Color::Green;
	default:
		break;
	}
}

static Direction RandDirection()
{
	int randDir = rand() % 8; //between 0 -7
	return (Direction)randDir;
}

static float CalculateEuclidianDistance(vec2 posA, vec2 posB)
{
	float x = (float)posA.x - (float)posB.x;
	float y = (float)posA.y - (float)posB.y;
	float dist;

	dist = pow(x, 2) + pow(y, 2);
	dist = sqrt(dist);

	return dist;
}

#endif
